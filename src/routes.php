<?php 

Route::get('/login', array('uses' => 'Timob\Sauth\controllers\SauthController@getLogin'));
Route::get('users/login', array('uses' => 'Timob\Sauth\controllers\SauthController@getLogin'));
Route::get('password/remind', array('uses' => 'Timob\Sauth\controllers\SauthController@getLogin'));
Route::get('users/register', array('uses' => 'Timob\Sauth\controllers\SauthController@getRegister'));
Route::get('users/logout', array('uses' => 'Timob\Sauth\controllers\SauthController@getLogout'));
Route::get('/logout', array('uses' => 'Timob\Sauth\controllers\SauthController@getLogout'));

Route::post('users/create', array('uses' => 'Timob\Sauth\controllers\SauthController@postCreate'));
Route::post('users/signin', array('uses' => 'Timob\Sauth\controllers\SauthController@postSignin'));




<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
		    	<h2>Login</h2>
		        <hr class="star-primary">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				{{ Form::open(array('url'=>'users/signin', 'class'=>'form-signin', 'role'=>"form")) }}
				<div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Username</label>
			    		{{ Form::text('username', null, array('class'=>'input-block-level', 'placeholder'=>'Username')) }}
			    	</div>
			    </div>
			    <div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Password</label>
			    		{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Password')) }}
			    	</div>
			    </div>
			    <div class="row">
			    	<div class="form-group col-xs-12">
			    		{{ Form::submit('Login', array('class'=>'btn btn-lg btn-success'))}}
			    	</div>
			    </div>
		{{ Form::close() }}

			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				{{ HTML::link('password/remind', 'Forgot my password') }}
			</div>
		</div>
	</div>
</section>



<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
		    	<h2>Please Register</h2>
		        <hr class="star-primary">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				{{ Form::open(array('url'=>'users/create', 'class'=>'form-signup', 'role'=>"form")) }}

				 
				    <ul>
				        @foreach($errors->all() as $error)
				            <li>{{ $error }}</li>
				        @endforeach
				    </ul>
				 
				    <div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
							<label for="name">Username</label>
				    		{{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Username')) }}
				    	</div>
				    </div>
				    <div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
							<label for="name">Email Address</label>
				    		{{ Form::text('email', null, array('class'=>'input-block-level', 'placeholder'=>'Email Address')) }}
				    	</div>
				    </div>
				    <div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
				    		<label for="name">Password</label>
				    		{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Password')) }}
				    	</div>
				    </div>
				    <div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
				    		<label for="name">Confirm Password</label>
				    		{{ Form::password('password_confirmation', array('class'=>'input-block-level', 'placeholder'=>'Confirm Password')) }}
				    	</div>
				    </div>
				    <div class="row">
				    	<div class="form-group col-xs-12">
				    		{{ Form::submit('Register', array('class'=>'btn btn-lg btn-success'))}}
				    	</div>
				    </div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</section>



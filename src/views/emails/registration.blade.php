<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Account Registration</h2>

		<div>
			To active your account, follow this link: {{ URL::to('users/verify/'.$hash) }}.
		</div>
	</body>
</html>

<?php namespace Timob\Sforum\controllers;

use \Illuminate\Routing\Controllers\Controller;
use \BaseController;
use \View;
use \Validator;
use \Input;
use \Redirect;
use \User;
use \Hash;
use \Verification;
use \Mail;
use \Auth;
use \Groups;
class HelperController extends BaseController {
	
	public static function getUserGroups($userid = '')
	{
		return (Auth::check()) ? explode(',',Auth::user()->groups) : array();
	}
	
	public static function isAllowedByGroup($groups = '')
	{
		if(!$groups){
			return true;
		}
	}

	public static function getGroups()
	{
		$groups = Groups::all();
		$gs = array();
		foreach ($groups as $g) {
			$gs[$g->id] = $g->name;
		}
		return $gs;
	}
}
<?php namespace Timob\Sauth\controllers;

use \Illuminate\Routing\Controllers\Controller;
use \BaseController;
use \View;
use \Validator;
use \Input;
use \Redirect;
use \User;
use \Hash;
use \Verification;
use \Mail;
use \Auth;

class SauthController extends BaseController {
	public function __construct() {
	    $this->beforeFilter('csrf', array('on'=>'post'));
	    $this->beforeFilter('auth', array('only'=>array('getDashboard')));
	}

	public function getVerify($hash='')
	{
		if(!$hash){
			return Redirect::to('users/register')->with('message', 'something went wrong, please re-register.');   
		}
		$v = Verification::where('hash', '=', trim($hash))->first();
		$twodays = 60*60*24*2;
		if((time() - $twodays) > strtotime($v->created_at)){
			$u = User::where('id','=',$v->user)->first();
			$u->delete();
			return Redirect::to('users/register')->with('message', 'Your verification code is no longer valid, please re-register.');
		}
		$u = User::where('id','=',$v->user)->first();
		$u->active = 1;
		$u->save();
		return Redirect::to('users/login')->with('message', 'Your verification was succesfull, you may login now.');

	}

	public function getRegister() {
		return View::make('sauth::users.register');
	}

	public function postCreate() {
        $rules = array(
		    'username'=>'required|alpha_num|min:2',
		    'email'=>'required|email|unique:users',
		    'password'=>'required|alpha_num|between:6,12|confirmed',
		    'password_confirmation'=>'required|alpha_num|between:6,12'
	    );
	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) {
        	$user = new User;
		    $user->username = Input::get('username');
		    $user->email = Input::get('email');
		    $user->password = Hash::make(Input::get('password'));
		    $user->active = 0;
		    $user->save();

		    $v = new Verification;
		    $v->user = $user->id;
		    $v->hash = md5(time().$user->id.$user->username);
		    $v->save();
		    
		    $data = array();
		    $data['hash'] = $v->hash;



		    Mail::send('sauth::emails.registration', $data, function($message)
		    {
				$message->from('noreply@sharemytrack.net', 'Share my track');
				$message->subject('[sharemytrack] - Account registration');
				$message->to(trim(Input::get('email')));
				
			});
		 
		    return Redirect::to('users/login')->with('message', 'Thanks for your registration!');
    	} else {
        	return Redirect::to('users/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
    	}
	}

	public function getLogin() {
	    return View::make('sauth::users.login');
	}

	public function postSignin() {
    	if (Auth::attempt(array('username'=>Input::get('username'), 'password'=>Input::get('password'), 'active' => 1))) {
		    return Redirect::to('/')->with('message', 'You are now logged in!');
		} else {
		    return Redirect::to('users/login')
		        ->with('message', 'Your username/password combination was incorrect')
		        ->withInput();
		}  
	}



	public function getLogout() {
	    Auth::logout();
	    return Redirect::to('users/login')->with('message', 'Your are now logged out!');
	}
}
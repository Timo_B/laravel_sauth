* In composer.json:
	
```
#!json

"repositories": [
        {
            "type": "vcs", 
            "url": "https://bitbucket.org/Timo_B/laravel_sauth.git"
        }
    ],
	"require": {
		"laravel/framework": "4.2.*",
		"timob/sauth": "dev-master"
	},
```


* composer update
* php artisan view:publish timob/sauth
* php artisan asset:publish timob/sauth
* in app/config/app.php:
* * add 'Timob\Sauth\SauthServiceProvider' to providers

* php artisan dump-autoload 

* php artisan migrate --package="timob/sauth"